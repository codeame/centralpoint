#include <EtherCard.h>

/**
This works with a Mega2650 if you override the CS pin in the sketch. The pin mapping is -

SO 50 (MISO)
SI 51 (MOSI)
CLK 52 (SCK)
CS 53 (SS)



 Sensor 1: 9.64v @ Vie 10 ENE 2014, 10:05 PM. Poco uso.
           9.57v @ Mar 14 ENE 2014, 11:38 PM.

**/

const int pinD0 = 39;
const int pinD1 = 37;
const int pinD2 = 35;
const int pinD3 = 33;
const int pinVT = 31;

const int pinToSiren = 2;

int s1_state;
int s2_state;
int s3_state;
int s4_state;
  

#define STATIC 1  // set to 1 to disable DHCP (adjust myip/gwip values below)

#if STATIC
// ethernet interface ip address
static byte myip[] = { 192,168,2,200 };
// gateway ip address
static byte gwip[] = { 192,168,1,1 };
#endif

// ethernet mac address - must be unique on your network
static byte mymac[] = { 0x74,0x69,0x69,0x2D,0x30,0x31 };

byte Ethernet::buffer[500]; // tcp/ip send and receive buffer

char page[] PROGMEM =
"HTTP/1.0 503 Service Unavailable\r\n"
"Content-Type: text/html\r\n"
"Retry-After: 600\r\n"
"\r\n"
"<html>"
  "<head><title>"
    "Service Temporarily Unavailable"
  "</title></head>"
  "<body>"
    "<h3>This service is currently unavailable</h3>"
    "<p><em>"
      "The main server is currently off-line.<br />"
      "Please try again later."
    "</em></p>"
  "</body>"
"</html>"
;


void beep(unsigned char delayms){
  analogWrite(pinToSiren, 20);      // Almost any value can be used except 0 and 255
                          
  delay(delayms);
  analogWrite(pinToSiren, 0);       // 0 turns it off
  delay(delayms); 
}

void setup(){
  pinMode(pinD0, INPUT);
  pinMode(pinD1, INPUT);
  pinMode(pinD2, INPUT);
  pinMode(pinD3, INPUT);
  pinMode(pinVT, INPUT);
  pinMode(pinToSiren, OUTPUT);
  
  s4_state = LOW;
  s3_state = LOW;
  s2_state = LOW;
  s1_state = LOW;
  
  Serial.begin(9600);
  Serial.println("\nSESY v1.0 by codea::me\n");
  
  if (ether.begin(sizeof Ethernet::buffer, mymac, 53) == 0) 
    Serial.println( "Failed to access Ethernet controller");
  else 
    Serial.println("Ethernet Controller Started...");
   
  beep(50); beep(50); beep(50);

   
#if STATIC
  ether.staticSetup(myip, gwip);
#else
  if (!ether.dhcpSetup())
    Serial.println("DHCP failed");
#endif

  ether.printIp("IP:  ", ether.myip);
  ether.printIp("GW:  ", ether.gwip);  
  ether.printIp("DNS: ", ether.dnsip);  
}

void loop(){
  //check for remote sensors.
  delay(10);
  int sensor1 = digitalRead(pinD0);
  int sensor2 = digitalRead(pinD1);
  int sensor3 = digitalRead(pinD2);
  int sensor4 = digitalRead(pinD3);
  
  
  //Testing for sensor 1
  if (sensor1 == HIGH) {
    if (s1_state == LOW) {
      Serial.println("MOTION DETECTED ON SENSOR 1 !");
      s1_state = HIGH;
      //HERE, SEND NOTIFICATION TO THE WEB FRONTEND / DATABASE SERVER.
      beep(100);
    }
  } else { // end if sensor1 == HIGH
    if (s1_state == HIGH)
    {
      Serial.println("MOTION ENDED ON sensor 1!");
      s1_state = LOW;
    }
  }
  
  //Testing for sensor 2
  if (sensor2 == HIGH) {
    if (s2_state == LOW) {
      Serial.println("MOTION DETECTED ON SENSOR 2 !");
      s2_state = HIGH;
      //HERE, SEND NOTIFICATION TO THE WEB FRONTEND / DATABASE SERVER.
      beep(100); beep(100);
    }
  } else { // end if sensor2 == HIGH
    if (s2_state == HIGH)
    {
      Serial.println("MOTION ENDED ON sensor 2!");
      s2_state = LOW;
    }
  }
  
  //Testing for sensor 3
  
  if (sensor3 == HIGH) {
    if (s3_state == LOW) {
      Serial.println("MOTION DETECTED ON SENSOR 3 !");
      s3_state = HIGH;
      //HERE, SEND NOTIFICATION TO THE WEB FRONTEND / DATABASE SERVER.
      beep(100); beep(100);beep(100);
    }
  } else { // end if sensor3 == HIGH
    if (s3_state == HIGH)
    {
      Serial.println("MOTION ENDED ON sensor 3!");
      s3_state = LOW;
    }
  }
  
  //Testing for sensor 4
  
  if (sensor4 == HIGH) {
    if (s4_state == LOW) {
      Serial.println("MOTION DETECTED ON SENSOR 4 !");
      s4_state = HIGH;
      //HERE, SEND NOTIFICATION TO THE WEB FRONTEND / DATABASE SERVER.
      beep(100); beep(100);beep(100); beep(100);
    }
  } else { // end if sensor4 == HIGH
    if (s4_state == HIGH)
    {
      Serial.println("MOTION ENDED ON sensor 4!");
      s4_state = LOW;
    }
  }
  
  
  
  
  
  
  
  
  
  // wait for an incoming TCP packet, but ignore its contents
  
  if (ether.packetLoop(ether.packetReceive())) {
    memcpy_P(ether.tcpOffset(), page, sizeof page);
    ether.httpServerReply(sizeof page - 1);
  }
}
